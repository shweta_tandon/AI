
print( "Loading human_robot.py" )

from copy import deepcopy

goal_state=[0,0,0]  ##final aim or goal: put all humans and robots on the other side of the river

# Printing name of the game
def hr_game_problem_info():
  print( "The human robot game" )


def possible_actions_for_game( state ):
	action = []
	print(state)
	
	if(state[0][2] == 1):
		if(state[0][0] == state[0][1]):
			if(state[0][0] == 2):
				action.append('A_HH')
			else:
				action.append('A_HR')
		if(state[0][0] < state[0][1] and state[0][0] == 1):
			action.append('A_HR')
							
		if(state[0][0] > state[0][1]):
			if(state[0][0] == 1 and state[0][1] == 0):
				action.append('A_H')
			if(state[0][0] - state[0][1] == 1):
				action.append('A_HR')
			if(state[0][0] - state[0][1] >= 2):
				action.append('A_HH')
	
	if(state[1][2] == 1):
		if(state[1][0] == state[1][1]):
			if(state[1][0] == 1 and state[1][1] == 1):
				action.append("B_H")
			if(state[1][0] >= 2):
				action.append("B_HR")
		if(state[1][0] > state[1][1] ):
			action.append("B_H")
		if( state[1][0] < state[1][1]):
			action.append("B_H")
		if(state[1][0] == 1):
			action.append("B_H")						
	return action

def successor_states( action, state ): #to make changes to the array in accordance with the possible action
	global newstate
	newstate = deepcopy(state)

	if action == ('A_HR'):   #human robot to left side
		newstate[0][0] = newstate[0][0] - 1
		newstate[0][1] = newstate[0][1] - 1
		newstate[0][2] = newstate[0][2] - 1
		newstate[1][0] = newstate[1][0] + 1
		newstate[1][1] = newstate[1][1] + 1
		newstate[1][2] = newstate[1][2] + 1
		
	elif action == ("B_HR"):    #human robot to right side
		newstate[1][0] = newstate[1][0] - 1
		newstate[1][1] = newstate[1][1] - 1
		newstate[1][2] = newstate[1][2] - 1
		newstate[0][0] = newstate[0][0] + 1
		newstate[0][1] = newstate[0][1] + 1
		newstate[0][2] = newstate[0][2] + 1

	if action == ('A_H'):     #human  
		newstate[0][0] = newstate[0][0] - 1
		newstate[0][1] = newstate[0][1]
		newstate[0][2] = newstate[0][2] - 1
		newstate[1][0] = newstate[1][0] + 1
		newstate[1][1] = newstate[1][1]
		newstate[1][2] = newstate[1][2] + 1


	elif action == ("B_H"):        #moving human 
		newstate[1][0] = newstate[1][0] - 1
		newstate[1][1] = newstate[1][1]
		newstate[1][2] = newstate[1][2] - 1
		newstate[0][0] = newstate[0][0] + 1
		newstate[0][1] = newstate[0][1]
		newstate[0][2] = newstate[0][2] + 1

	if action == ("A_HH"):    #moving two human 
		newstate[0][0] = newstate[0][0] - 2
		newstate[0][1] = newstate[0][1]
		newstate[0][2] = newstate[0][2] - 1
		newstate[1][0] = newstate[1][0] + 2
		newstate[1][1] = newstate[1][1]
		newstate[1][2] = newstate[1][2] + 1

	if action == ("B_HH"):   #two humans
		newstate[1][0] = newstate[1][0] - 2
		newstate[1][1] = newstate[1][1]
		newstate[1][2] = newstate[1][2] - 1
		newstate[0][0] = newstate[0][0] + 2
		newstate[0][1] = newstate[0][1]
		newstate[0][2] = newstate[0][2] + 1

	return newstate


def goal_test_function(state):
   # global goal_state
    return equivalent_states(state,goal_state)

def equivalent_states(state,goal_state ): #to check if the goal state and state obtained match
	
	if(state[0] != goal_state):
		return False
	elif(state[0] == goal_state):
		return True

def human_robot_function(humans_on_left,robots_on_left):
	##
	global LH,LR,LB,RH,RR,RB   
	print "humans on left side= ", humans_on_left
	print "robots on left side= ", robots_on_left   
	LB=1
	LR=robots_on_left
	LH=humans_on_left
	RR=0
	RH=0
	RB=0
	print "humans on right side= ", RH
	print "robots on right side= ", RR
	initial_state=([LH,LR,LB],[RH,RR,RB])

	
	return (None,hr_game_problem_info,initial_state,possible_actions_for_game,successor_states,goal_test_function)
