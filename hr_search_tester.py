## search_tester.py
## by Brandon Bennett --- 27/10/2009

## This is the top level file for running various search
## algorithms.

print( "Loading hr_search_tester.py" )

import sys
from tree          import *
from queue_search  import *
from human_robot import *

#make Python wait between search tests.
def wait():
      raw_input('<Press enter to continue>')


#to run the human robot game. 
#can change the numbers in the call for human_robot_function(x,y)
def run_tests_on_human_robot():
    search( human_robot_function(40,25),'depth_first',50000,['loop_check'])

run_tests_on_human_robot()
